# classification abjad character using MLP

Using this code, you can train and test your model with mlp(multilayer perceptron).
#
more code and text in https://hiddencluster.ir/
#
### run project (windows 10)
1. Within the project directory, create a Python virtual environment by typing:
- `virtualenv myprojectenv`
2. For install our project’s Python requirements, we need to activate the virtual environment. You can do that by typing:
- `source myprojectenv/bin/activate`
- `pip install -r requirements.txt`
3. Finally, run 'main_run.py'
4. You can also see the main code for model training in the 'main_algorithm_code.py'.

#
###### MAHDI RAHMANI
