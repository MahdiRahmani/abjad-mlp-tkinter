from tkinter.ttk import *
from tkinter import *
import PIL
from PIL import Image, ImageDraw
from pathlib import Path
import os
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow import expand_dims
import numpy as np
import tensorflow as tf
from tkinter import filedialog
import statistics
import online_train_algorithm


def predict():
    global image1, model, predict_show
    filename = f'image.png'
    image1.save(filename)
    directory_of_image = os.path.join(Path(__file__).resolve().parent, filename)

    img = load_img(
        directory_of_image,
        target_size=(32, 32),
        color_mode="grayscale",
    )
    img_array = img_to_array(img)
    img_array = expand_dims(img_array, 0)

    predictions = model.predict(img_array)
    score = tf.nn.softmax(predictions[0])
    abjad_list = ['الف', 'ب', 'د', 'ج']
    predict_show.set("حرف پیش بینی شده: {}".format(abjad_list[np.argmax(score)]))


def activate_paint(e):
    global lastx, lasty, cv
    cv.bind('<B1-Motion>', paint)
    lastx, lasty = e.x, e.y


def paint(e):
    global lastx, lasty, cv, draw
    x, y = e.x, e.y
    cv.create_line((lastx, lasty, x, y), width=50, capstyle=ROUND, smooth=TRUE, splinesteps=500)
    #  --- PIL
    draw.line((lastx, lasty, x, y), fill='black', width=50)
    lastx, lasty = x, y


def paint_window():
    global cv, draw, image1, predict_show
    newWindow = Toplevel(master)
    newWindow.geometry("500x500+740+30")
    newWindow.title("paint")
    newWindow.resizable(0, 0)

    cv = Canvas(newWindow, width=450, height=450, bg='white')
    cv.grid(row=1, columnspan=5)
    # --- PIL
    image1 = PIL.Image.new('RGB', (450, 450), 'white')
    draw = ImageDraw.Draw(image1)

    cv.bind('<1>', activate_paint)
    cv.pack(expand=YES, fill=BOTH)

    predict_show = StringVar()
    predict_label = Label(newWindow, textvariable=predict_show)
    predict_label.pack()

    btn_predict = Button(newWindow, text="پیش بینی کردن", command=predict)
    btn_predict.pack()


def set_directory():
    global directory_show
    directory = filedialog.askdirectory()
    directory_show.set(directory)


def train():
    global epoch_entry, val_accuracy_show, val_loss_show, train_accuracy_show, train_loss_show, directory_show

    history = online_train_algorithm.start_train(directory_show.get(), int(epoch_entry.get()))

    val_accuracy_show.set('val_accuracy: {}'.format(max(history['val_accuracy'])))
    val_loss_show.set('val_loss: {}'.format(min(history['val_loss'])))
    train_accuracy_show.set('train_accuracy: {}'.format(max(history['accuracy'])))
    train_loss_show.set('train_loss: {}'.format(min(history['loss'])))


def train_window():
    global epoch_entry, val_accuracy_show, val_loss_show, train_accuracy_show, train_loss_show, directory_show
    newWindow = Toplevel(master)
    newWindow.geometry("350x300+30+30")
    newWindow.title("train")
    newWindow.resizable(0, 0)

    string = """
    پوشه شما باید شامل چهار پوشه مجزا با نام های a,b,j,d باشد.
    در هر پوشه تصاویر برای آموزش قرار داده می شود.
    """
    label_description = Label(newWindow, text=string)
    label_description.pack()

    btn_directory = Button(newWindow, text="انتخاب پوشه", command=set_directory)
    btn_directory.pack()

    directory_show = StringVar()
    directory_label = Label(newWindow, textvariable=directory_show)
    directory_label.pack()

    label_epoch = Label(newWindow, text="تعداد epoch")
    label_epoch.pack()
    epoch_entry = Entry(newWindow)
    epoch_entry.pack()
    btn_directory = Button(newWindow, text="شروع آموزش", command=train)
    btn_directory.pack()

    val_accuracy_show = StringVar()
    val_accuracy_label = Label(newWindow, textvariable=val_accuracy_show)
    val_accuracy_label.pack()

    val_loss_show = StringVar()
    val_loss_label = Label(newWindow, textvariable=val_loss_show)
    val_loss_label.pack()

    train_accuracy_show = StringVar()
    train_accuracy_label = Label(newWindow, textvariable=train_accuracy_show)
    train_accuracy_label.pack()

    train_loss_show = StringVar()
    train_loss_label = Label(newWindow, textvariable=train_loss_show)
    train_loss_label.pack()


def set_directory1():
    global directory_show1, file_show1

    directory1 = filedialog.askdirectory()
    directory_show1.set(directory1)

    file1 = ""
    file_show1.set(file1)


def set_file1():
    global file_show1, directory_show1

    file1 = filedialog.askopenfile()
    file_show1.set(file1.name)

    directory1 = ""
    directory_show1.set(directory1)


def test():
    global file_show1, directory_show1, predict_show1

    if file_show1.get() != "" and directory_show1.get() == "":
        img = load_img(
            file_show1.get(),
            target_size=(32, 32),
            color_mode="grayscale",
        )
        img_array = img_to_array(img)
        img_array = expand_dims(img_array, 0)

        predictions = model.predict(img_array)
        score = tf.nn.softmax(predictions[0])
        abjad_list = ['الف', 'ب', 'د', 'ج']
        predict_show1.set("حرف پیش بینی شده: {}".format(abjad_list[np.argmax(score)]))

    elif file_show1.get() == "" and directory_show1.get() != "":
        predict_value = []
        for image in os.listdir(directory_show1.get()):
            img = load_img(
                os.path.join(directory_show1.get(), image),
                target_size=(32, 32),
                color_mode="grayscale",
            )
            img_array = img_to_array(img)
            img_array = expand_dims(img_array, 0)

            predictions = model.predict(img_array)
            score = tf.nn.softmax(predictions[0])
            predict_value.append(np.max(score))
        predict_show1.set("نرخ تشخیص: {}".format(statistics.mean(predict_value)))


def test_window():
    global directory_show1, file_show1, predict_show1
    newWindow = Toplevel(master)
    newWindow.geometry("680x200+30+400")
    newWindow.title("test")
    newWindow.resizable(0, 0)

    btn_directory1 = Button(newWindow, text="انتخاب پوشه", command=set_directory1)
    btn_directory1.pack()

    directory_show1 = StringVar()
    directory_label1 = Label(newWindow, textvariable=directory_show1)
    directory_label1.pack()

    btn_file1 = Button(newWindow, text="انتخاب تصویر", command=set_file1)
    btn_file1.pack()

    file_show1 = StringVar()
    file_label1 = Label(newWindow, textvariable=file_show1)
    file_label1.pack()

    btn_file1 = Button(newWindow, text="شروع فرآیند تست", command=test)
    btn_file1.pack()

    predict_show1 = StringVar()
    predict_label1 = Label(newWindow, textvariable=predict_show1)
    predict_label1.pack()


lastx, lasty, cv, draw, image1, predict_show, epoch_entry = None, None, None, None, None, None, None
val_accuracy_show, val_loss_show, train_accuracy_show, train_loss_show = None, None, None, None
directory_show, directory_show1, file_show1, predict_show1 = None, None, None, None

model = load_model('training_model')

master = Tk()
master.geometry("300x300+410+30")
master.title("mlp-abjad")
master.resizable(0, 0)

label = Label(master,
              text="mlp for abjad characters")
label.pack(pady=10)

label = Label(master,
              text="Mahdi Rahmani")
label.pack(pady=10)

label = Label(master,
              text="email: mahdirahmani1375@gmail.com")
label.pack(pady=10)

btn = Button(master,
             text="نوشتن حرف",
             command=paint_window)
btn.pack(pady=10)

btn = Button(master,
             text="آموزش شبکه با داده دلخواه",
             command=train_window)
btn.pack(pady=10)

btn = Button(master,
             text="تست دادگان ورودی",
             command=test_window)
btn.pack(pady=10)

mainloop()
