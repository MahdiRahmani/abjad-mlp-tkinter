from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import RMSprop


images = ImageDataGenerator(rescale=1. / 255, validation_split=0.2)
images_training = images.flow_from_directory(directory='dataset',
                                             color_mode="grayscale",
                                             subset="training",
                                             target_size=(32, 32),
                                             batch_size=32)
images_validation = images.flow_from_directory(directory='dataset',
                                               color_mode="grayscale",
                                               subset="validation",
                                               target_size=(32, 32),
                                               batch_size=32
                                               )

# creat model
model = Sequential()
model.add(Flatten())
# add first layer
model.add(Dense(760, activation='tanh'))
model.add(Dropout(0.4))

# add second layer
model.add(Dense(760, activation='tanh'))
model.add(Dropout(0.4))

# add output layer
model.add(Dense(4, activation='softmax'))

# compile the model
model.compile(loss='categorical_crossentropy', optimizer=RMSprop(), metrics=["accuracy"])

# fit model with train and validation data
early_stopping = EarlyStopping(monitor='val_loss', patience=20)

model_fit = model.fit(x=images_training, epochs=200, batch_size=32, verbose=1,
                      validation_data=images_validation, callbacks=[early_stopping])

print('val_accuracy: ', max(model_fit.history['val_accuracy']))
print('val_loss: ', min(model_fit.history['val_loss']))
print('train_accuracy: ', max(model_fit.history['accuracy']))
print('train_loss: ', min(model_fit.history['loss']))

model.save('training_model')

